package Day2;

public class Lab13 {
	public static void main(String[] args) {
		
		//No1.
		   int[][] twoD_Array = {
				     {1, 2, 3},{4, 5, 6, 7},{8, 9}
				   };
				   for (int[] row : twoD_Array) {
				     for (int element: row) {
				       System.out.println(element);
				    }
				   }

	  //No2.
				   int[][] two_Array = new int[2][2];
				   int result = 0;
				   
				   two_Array[0][0]=1;
				   two_Array[0][1]=2;
				   two_Array[1][0]=3;
				   two_Array[1][1]=4;
				   
				   for (int[] row : two_Array) {
					   result = result + row[row.length-1];
					   }

					   System.out.println(result);
				   
		
	}

}
