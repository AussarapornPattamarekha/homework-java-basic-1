package Homework;

public class HW3 {

	public static void main(String[] agru) {

		// draw9(3);
		// draw10(3);
		// draw11(3);
		// draw12(3);
		// draw13(3);
		// draw14(3);
		// draw15(3);
		// draw16(3);
		draw17(3);
	}

	public static void draw9(int n) {

		int num = 0;
		for (int i = 1; i <= n; i++) {

			System.out.println(num);

			num = num + 2;
		}

	}

	public static void draw10(int n) {

		int num = 2;
		for (int i = 1; i <= n; i++) {

			System.out.println(num);

			num = num + 2;
		}

	}

	public static void draw11(int n) {

		int num = 1;

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				System.out.print(num * j);

			}
			System.out.println();
			num++;
		}

	}

	static void draw12(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i == j) {
					System.out.print("_");
				} else {
					System.out.print("*");
				}

			}
			System.out.println();
		}
	}

	static void draw13(int n) {
		for (int i = n; i > 0; i--) {
			for (int j = 1; j <= n; j++) {
				if (i == j) {
					System.out.print("_");
				} else {
					System.out.print("*");
				}

			}
			System.out.println();
		}
	}

	static void draw14(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if (i >= j) {
					System.out.print("*");
				} else {
					System.out.print("_");
				}

			}
			System.out.println();
		}
	}

	static void draw15(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 0; j < n; j++) {
				if (i <= j) {
					System.out.print("_");
				} else {
					System.out.print("*");
				}

			}
			System.out.println();
		}
	}

	static void draw16(int n) {
		for (int i = n; i > 0; i--) {
			for (int j = 0; j < n; j++) {
				if (i <= j) {
					System.out.print("_");
				} else {
					System.out.print("*");
				}

			}
			System.out.println();
		}
	}

	static void draw17(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {

				if (i >= j) {

					System.out.print(i);
				} else {
					System.out.print("-");
				}

			}
			System.out.println();

		}

		for (int k = n-1; k > 0; k--) {
			for (int l = 1; l <= n; l++) {

				if (k < l ) {

					System.out.print("-");
				} else {
					System.out.print(k);
				}

			}
			System.out.println();
		}

	}

}
