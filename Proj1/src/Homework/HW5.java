package Homework;

public class HW5 {
	public static void main(String[] agru) {

		// draw18(3);
		// draw19(3);
		// draw20(3);
		// draw21(3);
		// draw22(3);
		// draw23(3);
		// draw24(3);
		draw25(3);

	}

	public static void draw18(int n) {

		for (int i = 1; i >= n; i++) {
			for (int j = 0; j <= n; j++) {
				if (j <= i) {
					System.out.print("-");
				} else {
					System.out.print("*");
				}
			}
			System.out.println();
		}

	}

	public static void draw19(int n) {

		for (int i = n; i > 0; i--) {
			for (int j = n; j > 0; j--) {

				if (i >= j) {
					System.out.print("*");
				} else {
					System.out.print("-");
				}
			}
			System.out.println();
		}

	}

	public static void draw20(int n) {

		for (int i = n; i > 0; i--) {
			for (int j = 1; j <= n; j++) {

				if (i <= j) {
					System.out.print("*");

				} else {
					System.out.print("-");
				}
			}
			System.out.println();
		}

		for (int k = n - 1; k > 0; k--) {
			for (int l = n; l > 0; l--) {

				if (k >= l) {
					System.out.print("*");
				} else {

					System.out.print("-");

				}

			}
			System.out.println();
		}
	}

	public static void draw21(int n) {

		int num = 1;
		for (int i = 0; i < n; i++) {
			for (int j = n - 1; j >= 0; j--) {

				if (i >= j) {
					System.out.print(num);
					num++;

				} else {
					System.out.print("-");
				}

			}

			System.out.println();

		}

		for (int k = n - 1; k > 0; k--) {
			for (int l = n; l > 0; l--) {

				if (k >= l) {
					System.out.print(num);
					num++;
				} else {

					System.out.print("-");

				}

			}
			System.out.println();
		}

	}

	static void draw22(int n) {
		for (int i = n; i > 0; i--) {
			for (int j = i; j > 1; j--) {

				System.out.print("-");
			}

			for (int j = i; j < n; j++) {
				System.out.print("*");
			}
			for (int j = i; j <= n; j++) {
				System.out.print("*");
			}
			for (int j = i; j > 1; j--) {
				System.out.print("-");
			}

			System.out.println();
		}
	}

	static void draw23(int n) {
		for (int i = n; i >= 1; i--) {
			for (int j = 1; j <= n - i; j++) {
				System.out.print("-");
			}
			for (int j = i; j <= 2 * i - 1; j++) {
				System.out.print("*");
			}
			for (int j = 0; j < i - 1; j++) {
				System.out.print("*");
			}
			for (int j = 1; j <= n - i; j++) {
				System.out.print("-");
			}
			System.out.println();
		}
	}

	static void draw24(int n) {
		for (int i = n; i > 0; i--) {
			for (int j = i; j > 1; j--) {
				System.out.print("-");
			}
			for (int j = i; j < n; j++) {
				System.out.print("*");
			}
			for (int j = i; j <= n; j++) {
				System.out.print("*");
			}
			for (int j = i; j > 1; j--) {
				System.out.print("-");
			}
			System.out.println();
		}
		for (int i = n - 1; i >= 1; i--) {
			for (int j = 1; j <= n - i; j++) {
				System.out.print("-");
			}
			for (int j = i; j <= 2 * i - 1; j++) {
				System.out.print("*");
			}
			for (int j = 0; j < i - 1; j++) {
				System.out.print("*");
			}
			for (int j = 1; j <= n - i; j++) {
				System.out.print("-");
			}
			System.out.println();
		}
	}

	public static void draw25(int n) {

		int number = 1;
		for (int i = n; i > 0; i--) {
			for (int j = i; j > 1; j--) {
				System.out.print("-");
			}
			for (int j = i; j < n; j++) {
				System.out.print(number);
				number++;
			}
			for (int j = i; j <= n; j++) {
				System.out.print(number);
				number++;
			}
			for (int j = i; j > 1; j--) {
				System.out.print("-");
			}
			System.out.println();
		}
		for (int i = n - 1; i >= 1; i--) {
			for (int j = 1; j <= n - i; j++) {
				System.out.print("-");
			}
			for (int j = i; j <= 2 * i - 1; j++) {
				System.out.print(number);
				number++;
			}
			for (int j = 0; j < i - 1; j++) {
				System.out.print(number);
				number++;
			}
			for (int j = 1; j <= n - i; j++) {
				System.out.print("-");
			}
			System.out.println();
		}
	}

}
