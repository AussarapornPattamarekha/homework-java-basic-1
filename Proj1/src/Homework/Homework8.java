package Homework;

public class Homework8 {
	public static void main(String[] args) {

		// Draw1(2);
		// Draw2(3);
		// Draw3(3);
		// Draw4(4);
		// Draw5(4);
		// Draw6(4);
		// Draw7(4);
		Draw8(3);

	}

	public static void Draw1(int n) {

		for (int i = 1; i <= n; i++) {
			String symbol = "*";
			System.out.print(symbol);

		}

	}

	public static void Draw2(int n) {

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {

				String symbol = "*";
				System.out.print(symbol);

			}

			System.out.println();

		}

	}

	public static void Draw3(int n) {

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {

				int num1 = j;
				System.out.print(num1);

			}

			System.out.println();

		}

	}

	public static void Draw4(int n) {
		int num1 = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {

				System.out.print(n - num1);
				num1++;
			}

			System.out.println();
			num1 = 0;
		}

	}

	public static void Draw5(int n) {
		int num1 = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {

				System.out.print(num1);

			}
			num1++;
			System.out.println();

		}

	}

	public static void Draw6(int n) {

		int num1 = n;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {

				System.out.print(num1);

			}
			num1--;
			System.out.println();

		}

	}

	public static void Draw7(int n) {

		int num2 = 1;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {

				System.out.print(num2);
				num2++;
			}

			System.out.println();

		}

	}

	public static void Draw8(int n) {

		int num2 = n * n;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {

				System.out.print(num2);
				num2--;
			}

			System.out.println();

		}

	}

}
