package Day1;

public class Lab1 {
	public static void main(String[] args) {

		/*--- No1.�ͧ Comment ���Ẻ Single Line ��� Multiple Line ---*/
		// Comment Single Line
		/* Comment Multiple Line */
		/*-----------------------end no1------------------------------*/

		/*------------ No2.��С�ȵ���û�������ҧ� --------------------*/
		int myNum = 25;
		boolean myBool = true;
		String myName = "Aussaraporn";
		/*-----------------------end no2------------------------------*/

		/*--- No3.Print �ء� ����� �ҡ��� 2 �͡�� ��ٻẺ �This is a String � + variable ---*/
		System.out.println("This is a String " + myNum);
		System.out.println("This is a String " + myBool);
		System.out.println("This is a String " + myName);
		/*-----------------------end no3------------------------------*/

		/*-------------Optional---------------*/
		// Optional No.1
		float myFloatNum = 5.99f;
		char myLetter = 'D';

		// Optional No.2
		double miles = 100;
		double MILES_TO_KILOMETER = 1.609;
		double kilometer;
		kilometer = miles * MILES_TO_KILOMETER;
		System.out.println("kilometer = " + kilometer);
		/*------------- end Optional---------------*/

	}

}
