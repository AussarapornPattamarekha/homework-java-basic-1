package Day1;

public class Lab2 {

	public static void main(String[] args) {

		// No1.
		bark();

		// No2.
		int oldInt = 5;
		float oldFloat = 8.9f;
		double oldDouble = 7.3d;
		char oldChar = 'A';

		// a. (float -> int)
		int newInt = (int) oldFloat;
		System.out.println("a.Float value: " + oldFloat);
		System.out.println("a.Int value: " + newInt);

		// b. (int -> float)
		float newFloat = oldInt;
		System.out.println("b.Int value: " + oldInt);
		System.out.println("b.Float value: " + newFloat);

		// c. (double -> float)
		float newFloat2 = (float) oldDouble;
		System.out.println("c.Double value: " + oldDouble);
		System.out.println("c.Float value: " + newFloat2);

		// d. (char -> int)
		int newInt2 = oldChar;
		System.out.println("d.Char value: " + oldChar);
		System.out.println("d.Int value: " + newInt2);

		// No3.
		// answer: Program run Error , ��û�С�� final �������
		// �繡�û�С����Ҩ��繤�Ҩ�����ա������¹�ŧ
		// �ҡ�ӡ������¹��ҵ���÷���С�� final ������Ǩ� compile ����ҹ
		final String hello = "Hello";
		// hello = "World";
		System.out.println(hello);

		// Optional
		// No1_Optional
		int myWeight = 74;
		float myFloatNum = 8.99f;
		char myLetter = 'A';
		boolean myBool = false;
		String myText = "Hello World";

		// No2_Optional
		double num1 = 124.23;
		int newNum1 = (int) num1;
		System.out.println(newNum1);

	}

	public static void bark() {
		// No1.
		String dogName = "Bever";
		System.out.println("The Dog name = " + dogName + " bark");

	}

}
