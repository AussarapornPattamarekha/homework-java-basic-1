package Day1;

public class Lab3 {

	public static void main(String[] args) {

		// No1.��С�� int i ����դ�� = 20
		// a. ���¡ i++ 5 ���� ���� print ��� i �͡��

		int i = 20;
		i++; // count 1
		++i; // count 2
		++i; // count 3
		++i; // count 4
		++i; // count 5
		System.out.println(i);

		
		// b. ���¡ --i 5 ���� ���� print ��� i �͡��
		--i; // count 1
		--i; // count 2
		--i; // count 3
		--i; // count 4
		--i; // count 5
		System.out.println(i);

		// No2.���ͧ�� && �Ѻ�����Ż����� float
		float f1 = 5.2f;
		float f2 = 19.3f;

		if ((f1 == 5) && (f2 == 19)) {
			System.out.println("Wrong");
		} else {
			System.out.println("Right");
		}

		// No3.���ͧ�� || �Ѻ�����Ż����� char
		char c1 = 'A';
		char c2 = 'Z';

		if (c1 == 'A' || c2 == 'Z') {
			System.out.println("English");
		} else {
			System.out.println("Maybe English");
		}

		// Optional
		// No1.
		OddEven.OddEven();

		// No2a.
		int firstA = 2;
		int secondA = 10;

		if (firstA != secondA)
			System.out.println(firstA + "!=" + secondA);

		// No2b.
		int firstB = 12;
		int secondB = 12;
		if (firstB == secondB)
			System.out.println(firstB + "==" + secondB);

	}

	public static class OddEven {
		public static void OddEven() {
			for (int value = 1; value < 6; value++) {
				if (value % 2 == 0) {
					System.out.println(value + "=" + "This is Even Number");
				} else {
					System.out.println(value + "=" + "This is Odd Number");
				}

			}
		}
	}

}
